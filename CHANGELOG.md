# Changelog

This project doesn't follow a versioning scheme.
The `master` branch is considered always up-to-date and can be reset at any time.

# Smartefact website

This is the [Smartefact website](https://smartefact.gitlab.io/), built with [JBake](https://jbake.org) and hosted at [GitLab](https://gitlab.com).

## License

This project is licensed under the [Creative Commons Attribution Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.

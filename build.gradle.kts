plugins {
    id("org.jbake.site") version "5.2.0"
}

jbake {
    version = "2.6.5"
    srcDirName = "src"
}

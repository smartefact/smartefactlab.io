title=Home
~~~~~~
Smartefact is a Belgian company dedicated to producing and providing top-notch quality software.

Some of our projects are open sourced under a flexible license and available on [GitLab](https://gitlab.com/smartefact).

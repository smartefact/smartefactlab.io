<#--
  - jQuery FreeMarker library
  - Author: Laurent Pireyn
  -->

<#import "cdnjs.lib.ftl" as cdnjs>

<#function jsUrl version>
    <#return cdnjs.url("jquery/${version}/jquery.min.js")>
</#function>

<#function slimJsUrl version>
    <#return cdnjs.url("jquery/${version}/jquery.slim.min.js")>
</#function>

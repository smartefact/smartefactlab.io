<#--
  - CDNJS FreeMarker library
  - Author: Laurent Pireyn
  -->

<#function url path>
    <#return "https://cdnjs.cloudflare.com/ajax/libs/${path}">
</#function>

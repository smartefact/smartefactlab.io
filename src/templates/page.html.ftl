<#include "site.inc.ftl">

<@page>
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column text-center">
        <header class="masthead mb-auto">
            <div class="inner">
                <h3 class="masthead-brand">Smartefact</h3>
                <nav class="nav nav-masthead justify-content-center">
                    <a class="nav-link active" href="${jbake.canonical()}">Home</a>
                    <a class="nav-link" href="https://gitlab.com/smartefact">GitLab</a>
                </nav>
            </div>
        </header>

        <main role="main" class="inner cover">
            ${content.body}
        </main>

        <footer class="mastfoot mt-auto">
            <div class="inner">
                <p>
                    Copyright &copy; ${published_date?string["yyyy"]} Smartefact -
                    Generated on <@h.time datetime=published_date>${published_date?datetime?string.long}</@h.time>
                </p>
            </div>
        </footer>
    </div>
</@page>

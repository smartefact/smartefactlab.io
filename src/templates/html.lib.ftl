<#--
  - HTML FreeMarker library
  - Author: Laurent Pireyn
  -->

<#macro attr name value skipEmpty=true>
    <#if (!skipEmpty || value?has_content)> ${name}="${value}"</#if><#t>
</#macro>

<#macro attrs hash skipEmpty=true>
    <#list hash?keys as name><@attr name=name value=hash[name] skipEmpty=skipEmpty /></#list><#t>
</#macro>

<#macro meta name content>
    <#if (content?has_content)><meta name="${name}" content="${content}"></#if><#lt>
</#macro>

<#macro link rel href>
    <#if (href?has_content)><link rel="${rel}" href="${href}"></#if><#lt>
</#macro>

<#macro script src>
    <script src="${src}"></script>
</#macro>

<#macro html lang="">
    <!DOCTYPE html>
    <html<@attr name="lang" value=lang />>
        <#nested>
    </html>
</#macro>

<#macro head title charset=.output_encoding!"" metaHash={} links=[]>
    <head>
        <#if (charset?has_content)><meta charset="${charset}" /></#if>
        <title>${title}</title>
        <#list metaHash?keys as name>
            <@meta name=name content=metaHash[name] />
        </#list>
        <#list links as linkTuple>
            <@link rel=linkTuple[0] href=linkTuple[1] />
        </#list>
        <#nested>
    </head>
</#macro>

<#macro time datetime>
    <time datetime="${datetime?datetime?string.iso}"><#nested></time><#t>
</#macro>

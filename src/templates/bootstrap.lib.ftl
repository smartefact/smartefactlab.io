<#--
  - Bootstrap FreeMarker library
  - Author: Laurent Pireyn
  -->

<#import "cdnjs.lib.ftl" as cdnjs>

<#function cssUrl version>
    <#return cdnjs.url("twitter-bootstrap/${version}/css/bootstrap.min.css")>
</#function>

<#function bundleJsUrl version>
    <#return cdnjs.url("twitter-bootstrap/${version}/js/bootstrap.bundle.min.js")>
</#function>

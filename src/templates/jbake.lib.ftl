<#--
  - JBake FreeMarker library
  - Author: Laurent Pireyn
  -->

<#function relPath path>
    <#return content.rootpath + path>
</#function>

<#function canonical content=content>
    <#return config.site_host + "/" + content.uri>
</#function>

<#function generator>
    <#return "JBake ${version} (FreeMarker v${.version})">
</#function>

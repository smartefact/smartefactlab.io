<#ftl output_format="HTML">

<#--
  - Site-specific FreeMarker include file
  - Author: Laurent Pireyn
  -->

<#import "bootstrap.lib.ftl" as bootstrap>
<#import "cdnjs.lib.ftl" as cdnjs>
<#import "html.lib.ftl" as h>
<#import "jbake.lib.ftl" as jbake>
<#import "jquery.lib.ftl" as jquery>

<#assign versions={
    "bootstrap": "4.5.0",
    "jQuery": "3.5.1"
}>

<#macro html>
    <@h.html lang=content.lang>
        <#nested>
    </@h.html>
</#macro>

<#macro head>
    <@h.head
        title="Smartefact | " + content.title
        metaHash={
            "viewport": "width=device-width, initial-scale=1, shrink-to-fit=no",
            "author": content.author!config.site_author!"",
            "generator": jbake.generator()
        }
        links=[
            ["canonical", jbake.canonical()],
            ["stylesheet", bootstrap.cssUrl(versions.bootstrap)],
            ["stylesheet", jbake.relPath("css/site.css")]
        ]
    >
        <#nested>
    </@h.head>
</#macro>

<#macro body>
    <body>
        <#nested>
    </body>
</#macro>

<#macro page>
    <@html>
        <@head />
        <@body>
            <#nested>
        </@body>
    </@html>
</#macro>
